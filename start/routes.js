'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
	return { topic: 'Assessment for node.js skills' }
})

// Api 

Route.group(() => {

   // Administration module
   Route.post('/register', 'AdministrationController.register')
   Route.get('/commonstudents', 'AdministrationController.common_students')
   Route.post('/suspend', 'AdministrationController.suspend')
   Route.post('/retrievefornotifications', 'AdministrationController.retrieve_for_notifications')

}).prefix('api/')
