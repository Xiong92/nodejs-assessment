'use strict'

const Validator = use('Validator')
const { validate,validateAll,is } = use('Validator')

const arrayEmail = async (data, field, message, args, get) => {

	const value = get(data, field)

	if(!value)
  	{
      throw 'Required'
    }

  	if(!Array.isArray(value))
  	{
      throw 'Not an array'
    }

    if(value.length==0)
  	{
      throw 'Empty array'
    }

    value.forEach((item,index,array)=>{
    	if(!is.email(item))
    	{
    		throw 'Invalid email'
    	}
    })

    return
}

const stringOrArrayEmail = async (data, field, message, args, get) => {

	const value = get(data, field)

	if(!value)
  	{
      throw 'Required'
    }

  	if(Array.isArray(value))
  	{
  		if(value.length==0)
	  	{
	      throw 'Empty array'
	    }

		value.forEach((item,index,array)=>{
	    	if(!is.email(item))
	    	{
	    		throw 'Invalid email'
	    	}
	    })
    }
    else
    {
    	if(!is.email(value))
    	{
    		throw 'Invalid email'
    	}
    }

    return
}

module.exports = {arrayEmail,stringOrArrayEmail}
