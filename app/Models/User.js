'use strict'

const Model = use('Model')

class User extends Model {

  static get table() {
    return 'user'
  }

  static get primaryKey() {
    return 'user_id'
  }

  static get incrementing() {
    return false
  }

  static get createdAtColumn() {
    return 'created_at'
  }

  static get updatedAtColumn() {
    return 'updated_at'
  }
  
}

module.exports = User
