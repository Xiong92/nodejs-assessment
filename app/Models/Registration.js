'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Registration extends Model {

  static get table() {
    return 'registration'
  }

  static get incrementing() {
    return false
  }

  static get createdAtColumn() {
    return 'created_at'
  }

  static get updatedAtColumn() {
    return null
  }
  
}

module.exports = Registration
