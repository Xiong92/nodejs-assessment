'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')
const Logger = use('Logger')

class CustomException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
  	// error log file
  	Logger.transport('file').info(error.message)
  	// 400 status code for bad request
    response.status(error.status).send({message:error.message})
  }
}

module.exports = CustomException
