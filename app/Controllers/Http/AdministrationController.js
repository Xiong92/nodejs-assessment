'use strict'

// library
const Config = use('Config')
const uuid = use('Uuid/v4')
const Database = use('Database')

// exception handler
const CustomException = use('App/Exceptions/CustomException')

// custom rule validation
const { validate,validateAll,is } = use('Validator')
const Validator = use('Validator')
const {arrayEmail,stringOrArrayEmail} = use('App/Validators/CustomValidator')
Validator.extend('arrayEmail', arrayEmail)
Validator.extend('stringOrArrayEmail', stringOrArrayEmail)

// db model
const User = use('App/Models/User')
const Role = use('App/Models/Role')
const Registration = use('App/Models/Registration')

class AdministrationController {

	/*
		Route: {Domain}/api/register
		Register one or more students to a specified teacher
	*/
	async register ({ request, response }) 
	{
		// parameter validation
    	let validation = await validateAll(request.all(), {
	      	teacher:'required|email',
	      	students:'arrayEmail'
	    })

    	if(validation.fails()) 
    	{
		  	throw new CustomException('Invalid HTTP parameter',400)
		}
		else
		{
			let param=request.all()
			let query
			let teacher_id
			let students_id=[]

			// begin db transaction
			const trx = await Database.beginTransaction()

			// find or create teacher user 
			query = await User.findOrCreate(
			  { email:param.teacher },
			  { user_id:uuid(),role_id:Config.get('app.role.teacher'),email:param.teacher,active:'y' }
			,trx)
			teacher_id=query.user_id

			// find or create student user
			for(let i=0;i<param.students.length;i++)
			{
				query = await User.findOrCreate(
				  { email:param.students[i] },
				  { user_id:uuid(),role_id:Config.get('app.role.student'),email:param.students[i],active:'y' }
				,trx)
				students_id.push(query.user_id)
			}

			// find of create registration
			for(let i=0;i<students_id.length;i++)
			{
				await Registration.findOrCreate(
				  { teacher_id:teacher_id,student_id:students_id[i] },
				  { teacher_id:teacher_id,student_id:students_id[i] }
				,trx)
			}

			// commit db change
			await trx.commit()

			return response.status(204).send()
		}

  	}

  	/*
		Route: {Domain}/api/commonstudents
		Retrieve a list of students common to a given list of teachers
	*/
  	async common_students ({ request, response }) 
  	{
  		// parameter validation
  		let validation = await validateAll(request.all(), {
	      	teacher:'stringOrArrayEmail'
	    })

    	if(validation.fails()) 
    	{
    		throw new CustomException('Invalid HTTP parameter',400)
		}
		else
		{
			let param=request.all()
			let query
			let teachers=[]
			let output={students:[]}

			// convert param into array form
			if(Array.isArray(param.teacher))
			{
				teachers=param.teacher
			}
			else
			{
				teachers.push(param.teacher)
			}

			// construct response data in specific format
			query=await Database.raw("select distinct c.email from registration a join user b on a.teacher_id=b.user_id join user c on a.student_id=c.user_id where b.role_id=? and b.email in(?)", [Config.get('app.role.teacher'),teachers])
			output.students=query[0].map(x=>x.email)

			return response.status(200).send(output)
		}
  	}

  	/*
		Route: {Domain}/api/suspend
		Suspend a specified student
	*/
  	async suspend ({ request, response }) 
  	{
  		// parameter validation
  		let validation = await validateAll(request.all(), {
	      	student:'required|email'
	    })

    	if(validation.fails()) 
    	{
    		throw new CustomException('Invalid HTTP parameter',400)
		}
		else
		{
			let param=request.all()
			let student

			// find the target student and update active to 'n'
			student=await User.findBy({email:param.student,role_id:Config.get('app.role.student')})

			if(student)
			{
				student.active='n'
				await student.save()
			}
			else
			{
				throw new CustomException('Student does not exist',400)
			}

			return response.status(204).send()
		}
  	}

  	/*
		Route: {Domain}/api/retrievefornotifications
		Retrieve a list of students who can receive a given notification
	*/
  	async retrieve_for_notifications ({ request, response }) 
  	{
  		// parameter validation
  		let validation = await validateAll(request.all(), {
	      	teacher:'required|email',
	      	notification:'required'
	    })

    	if(validation.fails()) 
    	{
    		throw new CustomException('Invalid HTTP parameter',400)
		}
		else
		{
			let param=request.all()
			let query
			let teacher
			let student=[]
			let student_list=[]
			let notification
			let output={recipients:[]}

			// get hashtag active student list
			notification=param.notification.split(' ')
			for(let i=0;i<notification.length;i++)
			{
				if(notification[i].charAt(0)=='@')
				{
					query=await User.findBy({email:notification[i].substr(1),role_id:Config.get('app.role.student'),active:'y'})
					if(query)
					{
						student.push(notification[i].substr(1))
					}
				}
			}

			// check whether teacher's email is valid
			teacher=await User.findBy({email:param.teacher,role_id:Config.get('app.role.teacher')})
			if(teacher)
			{
				// combine registered and hashtag active student and remove duplication
				query=await Database.raw("select c.email from registration a join user b on a.teacher_id=b.user_id join user c on a.student_id=c.user_id where c.active='y' and b.email=?", [param.teacher])
				student_list=(query[0].map(x=>x.email)).concat(student)
				output.recipients=student_list.filter((item, index)=>student_list.indexOf(item) >= index)

				return response.status(200).send(output)
			}
			else
			{
				throw new CustomException('Teacher does not exist',400)
			}
		}
  	}

}

module.exports = AdministrationController
