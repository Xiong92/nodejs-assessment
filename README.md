# NodeJS Assessment

This is the example project to assess proficiency of NodeJS skills

This project is developed by using following technology:
1. AdonisJS framework (https://adonisjs.com/)
2. Mysql
3. Postman 

## Setup

1) Make sure the pc has install the latest version of node.js and mysql
2) Run 'npm i -g @adonisjs/cli' for install adonisJS cli
3) Git clone or download the project source code
4) Run 'npm install' from project root path to grab all the dependencies
5) Create a mysql database named as 'assessment' 
6) Run the sql query file from the project './database/assessment.sql' folder
7) Modify the root .env file database credential accordingly
8) Run 'adonis serve --dev' command at the project root path
9) Project will run in localhost with 3333 port number 'http://127.0.0.1:3333'
10) Enjoy api testing

## Unit Testing

Get the postman collection with included unit testing criteria from below link<br/>
https://documenter.getpostman.com/view/5652650/RWguvwGC

## Project Structure Brief Explained

AdonisJS is based on MVC architecture

app<br/>
Controllers - Storing all the controller files (business logic)<br/>
Models - Storing all the database table model files (ORM)<br/>
Exceptions - Storing any custom error exception handler logic<br/>
Validators - Storing any custom validator rules

database<br/>
\- Grab the assessment.sql here

log<br/>
\- Viewing system error log here

start<br/>
\- Can adjust route config here

.env<br/>
\- Adjust project environment variable here

## Remark

Response status code:<br/><br/>
200,204 - Success<br/>
400 - Bad request ( Handled error )<br/>
500 - Internal server error ( Unexpected error )<br/>

\* Any occur error message will automatic store in error log file for checking purpose next time




